<?php


namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Exception;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{

    public function index(){
        return view('Auth/login');
    }

    /**
     * Logging  process
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logIn( LoginRequest $request){

        $credentials = $request->except('_token');
        if (Auth::attempt($credentials))
        {
            return redirect()->intended('/home');
        }
        else
        {
            return redirect()
                ->back()
                ->with('user_not_found' ,'Email are password are wrong');
        }
    }
    /**
     * Redirect the user to the Provider authentication page.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect('/home');
        }

        $authUser = $this->findOrCreateUser($user,$provider);

        Auth::login($authUser, true);

        return redirect('home');

    }

    /**
     * Create User with socalite
     * @param $user
     * @param $provider
     * @return mixed
     */
    private function findOrCreateUser($user,$provider)
    {

        if ($authUser = User::where('provider_id', $user->id)->first()) {
            return $authUser;
        }
        if ($existsEmail = User::where('email',$user->email)->first()){
            $email = null;
        }else{
            $email = $user->email;
        }
       if ($user->name !==  null){
           $name = $user->name;
       }else{
           $name = $user->nickname;
       }

        return User::create([
            'username' => $name,
            'email' => $email,
            'provider_id' => $user->id,
            'provider'   => $provider
        ]);
    }

    /**
     * Log out
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logOut(){
        Auth::logout();
        return redirect(route('login.index'));
    }

}
