<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * return view for registration
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  public function index(){
      return view('auth/registration');
  }

    /**
     * User Create process
     * @param RegisterRequest $request
     */
  public function createUser( RegisterRequest $request){
      User::create([
         'username' => $request->username,
         'email'    => $request->email,
         'password' => Hash::make($request->password)
      ]);
      return redirect()
          ->route('login.index')
          ->with('success','You are successfully registered');
  }
}
