<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Routes for Quests

Route::group(['Middleware' => 'guest'],function (){

    //Registration routes
    Route::get('/Registration', 'Auth\RegisterController@index')->name('registration.index');
    Route::post('/Registration.create','Auth\RegisterController@createUser')->name('registration.create');

    //Login routes
    Route::get('/','Auth\LoginController@index')->name('login.index');
    Route::post('/Login.store','Auth\LoginController@logIn')->name('login.store');

//Login Socialite
    Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
});

//Routes for authenticated users
Route::group(['middleware' => 'auth'],function(){

    // Home page redirect route
    Route::get('/home','HomeController@index')->name('home.index');

    // Log Out Route
    Route::get('/logOut','Auth\LoginController@logOut')->name('logOut');

});
