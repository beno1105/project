@extends('layouts.app')
@section('title') Login @endsection
@section('content')
<div class="alert alert-info">
    <h1>
        {{ auth()->user()->username }}
    </h1>
    <h2>
        {{auth()->user()->id}}
    </h2>
    <p>
        {{ auth()->user()->email }}
    </p>
    <a href="{{ route('logOut') }}"> Log Out</a>
</div>
@endsection
